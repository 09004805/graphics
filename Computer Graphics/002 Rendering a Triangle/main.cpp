#include <graphics_framework.h>
#include <glm\glm.hpp>
#include <ctime>

using namespace std;
using namespace graphics_framework;
using namespace glm;



// Define struct to hold info on all created geometry
struct sceneObject
{
	mesh mesh;

	texture tex;

	vec3 pos;
	
	vector<sceneObject*> children;
};

// Create dictionary of created sceneObjects
map<string, sceneObject> objects;
// Create effect
effect eff;
// Used in terrain gen
vector<float> heightmap;
// 'Player' cam
free_camera cam;
// Vars for setting cursor position, move speed and turn sensitivity
double cursor_x = 0.0;
double cursor_y = 0.0;
float lookSensitivity = 0.002f;
float camMoveSpeed = 0.5f;

// To add texture: right click on solution, add existing item, find file!

// Takes bmp file, reads data and outputs array of floats used in terrain gen
vector<float> ReadHeightMap(char* filename)
{
	// Create pointer to file
	FILE* f = fopen(filename, "rb");
	// To read header
	unsigned char head[54];
	fread(head, sizeof(unsigned char), 54, f);
	// Set width & height
	int width = *(int*)&head[18];
	int height = *(int*)&head[22];
	
	int padded_row = (width * 3 + 3) & (~3);
	unsigned char* input = new unsigned char[padded_row];
	vector<float> output;

	for (int i = 0; i < height; i++)
	{
		fread(input, sizeof(unsigned char), padded_row, f);
		for (int j = 0; j < width * 3; j += 3)
		{
			output.push_back((float)input[j] / 255.0f);
		}
	}
	// Close file
	fclose(f);

	return output;
}

// Generates terrain map from given bmp data
void Terrain()
{
	// Set to raise/lower entire mesh
	float displaceY = 25.0f;

	// Create emtpy scene object to store generated terrain info
	sceneObject caveCeiling;
	// Create array for texture coordinates
	vector<vec2> textureMap;
	// Distance between quads (set to 5 once lighting is in place)
	float tileSize = 0.4;
	// Positions of vectors to be created
	vector<vec3> terrain;
	// 
	float heightMultiplier = 50.0f;
	// Resolution
	int heightmapSize = 2048;
	// Changes mesh stretch
	int sizeMap = 64;
	//
	int sizeMultiplier = heightmapSize / (sizeMap + 1);
	// Read in height map
	heightmap = ReadHeightMap("..\\resources\\textures\\caveceilinghm.bmp");
	//
	for (int i = 0; i < sizeMap; i++)
	{
		for (int j = 0; j < sizeMap; j++)
		{
			float x = i - (sizeMap / 2);
			float z = j - (sizeMap / 2);
			float* height = new float[4];
			height[0] = 0.5f * (float)heightmap[(j * sizeMultiplier) + ((i * sizeMultiplier) * heightmapSize)];
			height[1] = 0.5f * (float)heightmap[((j + 1) * sizeMultiplier) + ((i * sizeMultiplier) * heightmapSize)];
			height[2] = 0.5f * (float)heightmap[(j * sizeMultiplier) + (((i + 1) * sizeMultiplier) * heightmapSize)];
			height[3] = 0.5f * (float)heightmap[((j + 1) * sizeMultiplier) + (((i + 1)* sizeMultiplier) * heightmapSize)];

			// Create first triangle of quad
			terrain.push_back(vec3(x + (tileSize), (displaceY + heightMultiplier * -height[2]), z - (tileSize)));
			terrain.push_back(vec3(x - (tileSize), (displaceY + heightMultiplier * -height[1]), z + (tileSize)));
			terrain.push_back(vec3(x - (tileSize), (displaceY + heightMultiplier * -height[0]), z - (tileSize)));
			
			// Second tri of quad
			terrain.push_back(vec3(x + (tileSize), (displaceY + heightMultiplier * -height[2]), z - (tileSize)));
			terrain.push_back(vec3(x + (tileSize), (displaceY + heightMultiplier * -height[3]), z + (tileSize)));
			terrain.push_back(vec3(x - (tileSize), (displaceY + heightMultiplier * -height[1]), z + (tileSize)));

				// Add texure coords - first tri
				textureMap.push_back(vec2(0.0f, 0.0f));
				textureMap.push_back(vec2(1.0f, 0.0f));
				textureMap.push_back(vec2(1.0f, 1.0f));

				// Second tri tex coords
				textureMap.push_back(vec2(1.0f, 1.0f));
				textureMap.push_back(vec2(0.0f, 1.0f));
				textureMap.push_back(vec2(0.0f, 0.0f));

			// Delete and clear height
			delete height;
			height = NULL;
		}
	}
	// Create geom to hold created terrain
	geometry generatedTerrain;
	// Add array of positions created
	generatedTerrain.add_buffer(terrain, BUFFER_INDEXES::POSITION_BUFFER);
	// Apply texture coords
	generatedTerrain.add_buffer(textureMap, BUFFER_INDEXES::TEXTURE_COORDS_0);
	// Set emtpy scene object to created geom
	caveCeiling.mesh.set_geometry(generatedTerrain);	
	// Set texture field of object
	caveCeiling.tex = texture("..\\resources\\textures\\cavefloor.jpg");
	// Add to dictionary
	objects["caveCeiling"] = caveCeiling;

}






bool initialise()
{	
	// Grabs current mouse position, updates to cursor position variables 
	glfwGetCursorPos(renderer::get_window(), &cursor_x, &cursor_y);
	// Disables the cursor
	glfwSetInputMode(renderer::get_window(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	return true;
}


bool load_content()
{
	// Generate cave ceiling from height map
	Terrain();
	

	// Floor dimensions
	unsigned int width = 100;
	unsigned int depth = 100;
	// Create empty sceneObject to hold floor
	sceneObject floor;
	// Set floor mesh to newly created plane
	floor.mesh = mesh(geometry_builder::create_plane(width, depth));
	// Set floor texture
	floor.tex = texture("..\\resources\\textures\\dungeonfloor.jpg");
	// Set floor position
	floor.pos = vec3(0.0f, 0.0f, 0.0f);
	floor.mesh.get_transform().translate(floor.pos);
	// Add floor object to object dictionary
	objects["floor"] = floor;



	// Pillar dimensions
	unsigned int slices = 10;
	unsigned int stacks = 1;
	vec3 pillarScale(1.0f, 15.0f, 1.0f);
	// Create empty sceneObject to hold pillar
	sceneObject pillar;
	// Set pillar mesh to newly created cylinder
	pillar.mesh = mesh(geometry_builder::create_cylinder(stacks, slices, pillarScale));
	// Set pillar texture
	pillar.tex = texture("..\\resources\\textures\\pillar.jpg");
	// Set pillar position
	pillar.pos = vec3(0.0f, 7.5f, 0.0f);
	pillar.mesh.get_transform().translate(pillar.pos);
	// Add pillar object to dictionary
	objects["pillar"] = pillar;

	//

	// Create empty sceneObject to hold crate
	sceneObject crate;
	// Set crate mesh to newly created box
	crate.mesh = mesh(geometry_builder::create_box());
	// Set texture
	crate.tex = texture("..\\resources\\textures\\crate.jpg");
	// Set position 
	crate.pos = vec3(5.0f, 0.5f, 0.0f);
	crate.mesh.get_transform().translate(crate.pos);
	// Add crate object to dictionary
	objects["crate"] = crate;

	




	// Load shaders
	eff.add_shader("..\\resources\\shaders\\simple_texture.vert", GL_VERTEX_SHADER);
	eff.add_shader("..\\resources\\shaders\\simple_texture.frag", GL_FRAGMENT_SHADER);

	// Build effect
	eff.build();

	// Set camera properties
	cam.set_position(vec3(10.0f, 10.0f, 10.0f));
	cam.set_target(vec3(-100.0f, 0.0f, -100.0f));
	auto aspect = static_cast<float>(renderer::get_screen_width()) / static_cast<float>(renderer::get_screen_height());
	cam.set_projection(quarter_pi<float>(), aspect, 2.414f, 1000.0f);
	return true;
}

bool update(float delta_time)
{
	// To store current cursor position
	double currentX;
	double currentY;

	// Get current cursor position
	glfwGetCursorPos(renderer::get_window(), &currentX, &currentY);

	// Create delta variables
	double deltaX;
	double deltaY;

	// Calculate delta values
	deltaX = currentX - cursor_x;
	deltaY = currentY - cursor_y;

	// Update current cursor position
	cursor_x = currentX;
	cursor_y = currentY;


	// Multiply by ratios to lower sensitivity MAKE GLOBAL VAR!!!
	deltaX *= lookSensitivity;
	deltaY *= lookSensitivity;

	// Set camera mouse control inversion (remove - to invert)
	cam.rotate(deltaX, -deltaY);
	
	
	vec3 camPos(0.0f);
	
	// Set camera controls
	// Move forward
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_UP))
		camPos -= camMoveSpeed * (normalize(cam.get_position() - cam.get_target()));
	// Move backward
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_DOWN))
		camPos += camMoveSpeed * (normalize(cam.get_position() - cam.get_target()));
	// Strafe left
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_LEFT))
		camPos += camMoveSpeed * cross((normalize(cam.get_position() - cam.get_target())), vec3(0.0f, 1.0f, 0.0f));
	// Strafe right
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_RIGHT))
		camPos -= camMoveSpeed * cross((normalize(cam.get_position() - cam.get_target())), vec3(0.0f, 1.0f, 0.0f));

	cam.set_position(cam.get_position() + camPos);

	// Update the camera
	cam.update(delta_time);
	return true;
}

bool render()
{
	// Render cave meshes
	for (auto &e : objects)
	{
		auto m = e.second;
		// Bind effect
		renderer::bind(eff);
		// Create MVP matrix
		auto M = m.mesh.get_transform().get_transform_matrix();
		auto V = cam.get_view();
		auto P = cam.get_projection();
		auto MVP = P * V * M;
		// Set MVP matrix uniform
		glUniformMatrix4fv(
			eff.get_uniform_location("MVP"), // Location of uniform
			1, // Number of values - 1 mat4
			GL_FALSE, // Transpose the matrix?
			value_ptr(MVP)); // Pointer to matrix data


			renderer::bind(m.tex, 0);
				
		glUniform1i(eff.get_uniform_location("tex"), 0);
		// Render mesh
		renderer::render(m.mesh);
	}

	
	glUniform1i(eff.get_uniform_location("tex"), 0);

	
	return true;




}

void main()
{
	// Create application
	app application;
	// Set load content, update and render methods
	application.set_load_content(load_content);
	application.set_update(update);
	application.set_render(render);

	// Get initial cursor position
	initialise();

	// Run application
	application.run();
	
}