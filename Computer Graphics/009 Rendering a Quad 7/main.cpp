#include <graphics_framework.h>
#include <glm\glm.hpp>

using namespace std;
using namespace graphics_framework;
using namespace glm;

geometry geom;
effect eff;
target_camera cam;

float total_time = 0.0f;


bool load_content()
{
	// Set geometry type to triangle fan
	geom.set_type(GL_TRIANGLE_FAN);
	// Positions
	vector<vec3> positions
	{
		vec3(0.0f, 0.0f, 0.0f)
		// changes
		/*
		vec3(2.0f, 1.0f, 0.0f),
		vec3(1.0f, 2.0f, 0.0f),
		vec3(0.0f, 2.5f, 0.0f),
		vec3(-1.0f, 2.0f, 0.0f),
		vec3(-2.0f, 1.0f, 0.0f)
		*/
	};

	// Create circle from triangle fan
	// Increase points value for smoother circle edge
	float points = 360;
	// +1 on points completes the circle - lacks one triangle otherwise
	for (int i = 0; i < points + 1; i++)
	{
		// Declare matrix
		mat4 rot;
		rot = glm::rotate(rot, -pi<float>()*2.0f / points * i, vec3(0, 0, 1));
		vec4 point = vec4(0.0f, 3.0f, 0.0f, 1) * rot;
		// cout << point.x << ", "<< point.y << ", "<< point.z << endl;
		positions.push_back(vec3(point.x, point.y, point.z));

	}
	// Colours
	vector<vec4> colours
	{
		vec4(1.0f, 0.0f, 0.0f, 1.0f),
		vec4(1.0f, 0.0f, 0.0f, 1.0f),
		vec4(1.0f, 0.0f, 0.0f, 1.0f),
		vec4(1.0f, 0.0f, 0.0f, 1.0f)
	};
	// Add to the geometry
	geom.add_buffer(positions, BUFFER_INDEXES::POSITION_BUFFER);

	glDisable(GL_CULL_FACE);
	glPolygonMode(GL_FRONT, GL_LINE);
	glPolygonMode(GL_BACK, GL_LINE);
	//geom.add_buffer(colours, BUFFER_INDEXES::COLOUR_BUFFER);

	// Load in shaders
	eff.add_shader(
		"..\\resources\\shaders\\basic.vert", // filename
		GL_VERTEX_SHADER); // type
	eff.add_shader(
		"..\\resources\\shaders\\basic.frag", // filename
		GL_FRAGMENT_SHADER); // type
	// Build effect
	eff.build();

	// Set camera properties
	cam.set_position(vec3(0.0f, 0.0f, 10.0f));
	cam.set_target(vec3(0.0f, 0.0f, 0.0f));
	auto aspect = static_cast<float>(renderer::get_screen_width()) / static_cast<float>(renderer::get_screen_height());
	cam.set_projection(quarter_pi<float>(), aspect, 2.414f, 1000.0f);
	return true;
}

bool update(float delta_time)
{
	//total_time += delta_time;

	mat4 rot;
	rot = glm::rotate(rot, -pi<float>() * total_time, vec3(0, 0, 1));
	vec4 point = vec4(0.0f, 3.0f, 0.0f, 1) * rot;
	vec3 pos = vec3(point.x, point.y, point.z);

	if (glfwGetKey(renderer::get_window(), GLFW_KEY_UP) == GLFW_PRESS){
		total_time += delta_time;
		
	}
	else if (glfwGetKey(renderer::get_window(), GLFW_KEY_DOWN) == GLFW_PRESS){
		total_time -= delta_time;
	}

	//cam.set_target(pos);
	cam.set_position(pos + vec3(0, 0, 10));
	// Update the camera
	cam.update(delta_time);
	return true;
}

bool render()
{
	// Bind effect
	renderer::bind(eff);
	// Create MVP matrix
	mat4 M(1.0f);
	auto V = cam.get_view();
	auto P = cam.get_projection();
	auto MVP = P * V * M;
	// Set MVP matrix uniform
	glUniformMatrix4fv(
		eff.get_uniform_location("MVP"), // Location of uniform
		1, // Number of values - 1 mat4
		GL_FALSE, // Transpose the matrix?
		value_ptr(MVP)); // Pointer to matrix data
	// Render geometry
	renderer::render(geom);
	// Move camera back along z axis 
	//cam.set_position(cam.get_position() + vec3(0,0,-0.1f));
	return true;
}

void main()
{
	// Create application
	app application;
	// Set load content, update and render methods
	application.set_load_content(load_content);
	application.set_update(update);
	application.set_render(render);
	// Run application
	application.run();
}