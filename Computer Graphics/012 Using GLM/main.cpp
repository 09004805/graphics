#include <iostream>
#include <glm\glm.hpp>
#include <glm\gtc\quaternion.hpp>
#include <glm\gtc\constants.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtx\euler_angles.hpp>
#include <glm\gtx\projection.hpp>

using namespace std;
using namespace glm;

int main()
{
	mat4 m(1.0f); // Identity matrix, where all diagonal values are set to 1
	mat4 o(1.0f);
	mat3 n(mat4(1.0f)); // Converts mat4 to mat3, as an identity matrix

	mat4 sum = m + o;

	mat4 scale = 5.0f * m;
	mat4 invscale = m / 5.0f;

	// Matrix * Vector
	mat4 T;
	vec3 u;
	// Can't multiply vector by mat4, must convert to 4d vector
	vec4 v = T * vec4(u, 1.0f);
	//Answer v = mat4 * converted vec3 u. Answer is still a 4d vector
	// Return answer converted back to vec3 via:
	vec3 w = vec3(T * vec4(u, 1.0f));

	mat4 L = translate(mat4(1.0f), vec3(1.0f, 2.0f, 3.0f));
}